# application

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.


#setup
1. setup npm
2. Run npm install -g grunt-cli 
3. cd repository
4. rm -rfv node_modules && npm cache clean && npm install && bower install
5. change the url in baseurlservice.js to your running rails server
6. grunt serve



## Build & development

Run `grunt` for building and `grunt serve` for preview.

