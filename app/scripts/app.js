'use strict';

/**
 * @ngdoc overview
 * @name TopicManagementSystem
 * @description
 * # TopicManagementSystem
 *
 * Main module of the application.
 */
angular
    .module('TopicManagementSystem', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch'
    ])
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/topic.html',
                controller: 'TopicCtrl',
                controllerAs: 'topic',
                resolve: {
                    topicData: ['topicManagementService', function(topicManagementService) {
                        return topicManagementService.getTopics().then(function(response) {
                            return response;
                        });

                    }]
                }
            })
            .when('/comment/:topic_id', {
                templateUrl: 'views/comment.html',
                controller: 'CommentCtrl',
                controllerAs: 'comment'
                
            })
            .otherwise({
                redirectTo: '/'
            });
    });
