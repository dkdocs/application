'use strict';

/**
 * @ngdoc function
 * @name TopicManagementSystem.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the TopicManagementSystem
 */
angular.module('TopicManagementSystem')
    .controller('CommentCtrl', ['topicManagementService', '$routeParams', '$scope', function(topicManagementService, $routeParams, $scope) {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];

        $scope.loading = false;
        $scope.comments = [];

        $scope.$on('$routeChangeSuccess', function() {
            topicManagementService.getComments($routeParams.topic_id).then(function(response) {
                if (response.data.meta.code === 200) {
                    $scope.comments = response.data.comments;
                }
            });
        });

        $scope.voteUp = function(comment) {

            topicManagementService.updateScore(comment.id, 1).then(function(response) {
                comment.score = response.data.score;
            });

        };
        $scope.voteDown = function(comment) {
            topicManagementService.updateScore(comment.id, -1).then(function(response) {
                comment.score = response.data.score;
            });
        };
        $scope.loadCompleteComment = function(comment) {
            topicManagementService.loadCompleteComment(comment.id).then(function(response) {
                if (response.data.meta.code === 200) {
                    comment.message = response.data.message;
                    comment.more = true;
                }
            });

        };

        $scope.more = function() {
            $scope.loading = true;
            topicManagementService.getComments($routeParams.topic_id, $scope.comments.length - 1).then(function(response) {

                console.log(response);
                console.log($scope.comments);
                for (var index in response.data.comments) {
                    $scope.comments.push(response.data.comments[index]);
                }
                $scope.loading = false;
            });
        };

        $scope.createComment = function(message, email, valid) {
            if (valid) {
                topicManagementService.createComment($routeParams.topic_id, message, email).then(function(response) {
                    if (response.data.meta.code === 200) {
                        $scope.comments.unshift(response.data.comment);
                        $scope.email = '';
                        $scope.message = '';
                        $scope.commentForm.$setPristine();
                        $scope.commentForm.$setUntouched();
                    }
                });
            }
        };

        $scope.getTopic = function() {
            topicManagementService.getTopic($routeParams.topic_id).then(function(response) {
                if (response.data.meta.code === 200) {
                    $scope.topic = response.data.topic;
                }
            });
        };

        $scope.getTopic();



    }]);
