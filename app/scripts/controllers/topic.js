'use strict';

/**
 * @ngdoc function
 * @name TopicManagementSystem.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the TopicManagementSystem
 */
angular.module('TopicManagementSystem')
  .controller('TopicCtrl', [ 'topicData', '$scope', function(topicData, $scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    console.log( topicData);
    $scope.topics = topicData.data;


  }]);
