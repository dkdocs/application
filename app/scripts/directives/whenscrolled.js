'use strict';

/**
 * @ngdoc directive
 * @name TopicManagementSystem.directive:whenScrolled
 * @description
 * # whenScrolled
 */
angular.module('TopicManagementSystem')
    .directive('whenScrolled', function() {
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {


                var raw = elem[0];

                // we load more elements when scrolled past a limit
                elem.bind('scroll', function() {
                    if (raw.scrollTop + raw.offsetHeight + 5 >= raw.scrollHeight) {
                        scope.loading = true;
                        scope.$apply(attrs.whenScrolled);
                    }
                });
            }
        };

    });
