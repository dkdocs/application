'use strict';

/**
 * @ngdoc service
 * @name TopicManagementSystem.baseUrlService
 * @description
 * # baseUrlService
 * Factory in the TopicManagementSystem.
 */
angular.module('TopicManagementSystem')
  .factory('baseUrlService', function () {
    
    var url = 'http://2f9d212d.ngrok.io';
    
    return {
      getBaseUrl: function () {
        return url;
      }
    };
  
  });
