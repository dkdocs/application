'use strict';

/**
 * @ngdoc service
 * @name TopicManagementSystem.topicManagementService
 * @description
 * # topicManagementService
 * Factory in the TopicManagementSystem.
 */


angular.module('TopicManagementSystem')
    .factory('topicManagementService', ['baseUrlService', '$http', function(baseUrlService, $http) {

        var factory = {};

        var BASE_URL = baseUrlService.getBaseUrl();

        factory.getTopics = function() {
            return $http({

                method: 'GET',
                url: BASE_URL + '/topics'
            });
        };

        factory.getComments = function(topic_id, after) {
            after = after >= 0 ? after : 0;
            return $http({
                method: 'GET',
                url: BASE_URL + '/topics/' + topic_id + '/comments?after=' + after
            });
        };

        factory.updateScore = function(comment_id, vote) {
            return $http({

                method: 'PUT',
                url: BASE_URL + '/comments/' + comment_id,
                data: {
                    vote: vote
                }

            });
        };

        factory.loadCompleteComment = function(comment_id) {
            return $http({
                method: 'GET',
                url: BASE_URL + '/comments/' + comment_id + '/complete_comment'
            });
        };

        factory.createComment = function(topic_id, message, email) {
            return $http({
                method: 'POST',
                url: BASE_URL + '/topics/' + topic_id + '/comments',
                data: {
                    message: message,
                    topic_id: topic_id,
                    email: email
                }
            });
        };

        factory.getTopic = function(topic_id){
           
            return $http({
                method: 'GET',
                url: BASE_URL + '/topics/' + topic_id
            });
        };



        return factory;

    }]);
