'use strict';

describe('Service: baseUrlService', function () {

  // load the service's module
  beforeEach(module('TopicManagementSystem'));

  // instantiate service
  var baseUrlService;
  beforeEach(inject(function (_baseUrlService_) {
    baseUrlService = _baseUrlService_;
  }));

  it('should do something', function () {
    expect(!!baseUrlService).toBe(true);
  });

});
