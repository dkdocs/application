'use strict';

describe('Service: topicManagementService', function () {

  // load the service's module
  beforeEach(module('TopicManagementSystem'));

  // instantiate service
  var topicManagementService;
  beforeEach(inject(function (_topicManagementService_) {
    topicManagementService = _topicManagementService_;
  }));

  it('should do something', function () {
    expect(!!topicManagementService).toBe(true);
  });

});
